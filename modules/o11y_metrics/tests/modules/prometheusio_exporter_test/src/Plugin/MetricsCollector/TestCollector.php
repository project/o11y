<?php

namespace Drupal\o11y_metrics_test\Plugin\MetricsCollector;

use Drupal\o11y_metrics\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dummy plugin for testing purposes.
 *
 * @MetricsCollector(
 *   id = "test",
 *   title = @Translation("Test Collector"),
 *   description = @Translation("A test collector.")
 * )
 */
class TestCollector extends BasePluginMetricsCollector {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @phpstan-ignore-next-line
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o11y_metrics.prometheus_bridge')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $namespace_name_help = [
      $this->getNamespace(),
      'total',
      $this->getDescription(),
    ];
    $this->promBridge->getGauge(...$namespace_name_help, ...[['foo']])->set(1234, ['bar']);
  }

}
