<?php

namespace Drupal\Tests\o11y_metrics\Unit\Cache;

use Drupal\pcb\Cache\PermanentDatabaseBackend;
use Drupal\Core\Cache\MemoryBackend;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;

/**
 * Proxy to MemoryBackend inheriting from PermanentDatabaseBackend.
 *
 * Used only in tests to respect check made in
 * Drupal\o11y_metrics\Prometheus\Storage\DrupalCache.
 */
class PermanentMemoryProxyBackend extends PermanentDatabaseBackend {

  /**
   * The wrapped cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $memoryBackend;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->memoryBackend = new MemoryBackend();
  }

  /**
   * {@inheritdoc}
   */
  public function get($cid, $allow_invalid = FALSE) {
    $cache = $this->memoryBackend->get($cid, $allow_invalid);
    return $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    $cache = $this->memoryBackend->getMultiple($cids, $allow_invalid);
    return $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = []) {
    return $this->memoryBackend->set($cid, $data, $expire, $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $items) {
    return $this->memoryBackend->setMultiple($items);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($cid) {
    return $this->memoryBackend->delete($cid);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $cids) {
    return $this->memoryBackend->deleteMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    return $this->memoryBackend->deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate($cid) {
    return $this->memoryBackend->invalidate($cid);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateMultiple(array $cids) {
    return $this->memoryBackend->invalidateMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags) {
    if ($this->memoryBackend instanceof CacheTagsInvalidatorInterface) {
      $this->memoryBackend->invalidateTags($tags);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateAll() {
    return $this->memoryBackend->invalidateAll();
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection() {
    return $this->memoryBackend->garbageCollection();
  }

  /**
   * {@inheritdoc}
   */
  public function removeBin() {
    return $this->memoryBackend->removeBin();
  }

}
