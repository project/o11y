<?php

namespace Drupal\Tests\o11y_metrics\Unit\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\o11y_metrics\Plugin\MetricsCollector\UserCount;

/**
 * @coversDefaultClass \Drupal\o11y_metrics\Plugin\MetricsCollector\UserCount
 * @group o11y_metrics
 */
class UserCountTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {
    $total = 12;
    $blocked = 5;
    $activated = $total - $blocked;
    $userQuery_tot = $this->getUserQueryMock($total);
    $userQuery_active = $this->getUserQueryMock($activated, [['status', TRUE]]);
    $userQuery_inactive = $this->getUserQueryMock($blocked, [['status', FALSE]]);
    $userQuery_editor = $this->getUserQueryMock(3, [['roles', 'editor']]);
    $userQuery_admin = $this->getUserQueryMock(5, [['roles', 'administrator']]);
    $userQuery_noroles = $this->getUserQueryMock(4, [['roles', NULL, 'IS NULL']]);

    $userTypeStorage = $this->prophesize(EntityStorageInterface::class);
    $userTypeStorage->getQuery()
      ->willReturn(
        $userQuery_tot,
        $userQuery_active,
        $userQuery_inactive,
        $userQuery_editor,
        $userQuery_admin,
        $userQuery_noroles
      );
    $roleTypeStorage = $this->prophesize(EntityStorageInterface::class);
    $roles = [
      'editor' => 'something',
      'administrator' => 'something',
    ];
    $roleTypeStorage->loadMultiple()->willReturn($roles);

    $configuration = [
      'enabled' => TRUE,
      'weight' => 0,
      'settings' => [],
    ];
    $definition = [
      'provider' => 'user_count',
      'description' => 'Test description',
    ];

    $collector = new UserCount(
      $configuration,
      'user_count',
      $definition,
      $this->prometheusBridge,
      $userTypeStorage->reveal(),
      $roleTypeStorage->reveal()
    );

    $collector->executeMetrics();

    $this->assertEquals(<<<EOD
# HELP drupal_user_count_total Test description
# TYPE drupal_user_count_total gauge
drupal_user_count_total 12
# HELP drupal_user_count_total_per_role Test description
# TYPE drupal_user_count_total_per_role gauge
drupal_user_count_total_per_role{role="administrator"} 5
drupal_user_count_total_per_role{role="editor"} 3
drupal_user_count_total_per_role{role="no_roles"} 4
# HELP drupal_user_count_total_per_status Test description
# TYPE drupal_user_count_total_per_status gauge
drupal_user_count_total_per_status{status="active"} 7
drupal_user_count_total_per_status{status="blocked"} 5
EOD, $this->prometheusBridge->render());
  }

  /**
   * Utility function.
   */
  protected function getUserQueryMock($count, array $conditions = []) {
    $userQuery = $this->prophesize(QueryInterface::class);
    $userQuery->accessCheck(FALSE)->willReturn($userQuery);
    foreach ($conditions as $condition) {
      $userQuery->condition(...$condition)->willReturn($userQuery);
    }
    $userQuery->count()->willReturn($userQuery);
    $userQuery->execute()->willReturn($count);
    return $userQuery;
  }

}
