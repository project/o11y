<?php

namespace Drupal\Tests\o11y_metrics\Unit\Plugin\MetricsCollector;

use Drupal\Tests\UnitTestCase;
use Drupal\o11y_metrics\Bridge\PrometheusBridge;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\o11y_metrics\MetricsCollectorManager;
use Drupal\o11y_metrics\Prometheus\Storage\DrupalCache;
use Drupal\Tests\o11y_metrics\Unit\Cache\PermanentMemoryProxyBackend;

/**
 * Common abstract class.
 */
abstract class AbstractTestBaseMetrics extends UnitTestCase {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * The promPHP bridge.
   *
   * @var \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface
   */
  protected $prometheusBridge;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $drupalCache = new DrupalCache(new PermanentMemoryProxyBackend());
    $prometheusBridge = new PrometheusBridge($drupalCache);
    $container = $this->prophesize(ContainerInterface::class);
    $mcm = $this->prophesize(MetricsCollectorManager::class);
    $container->get('o11y_metrics.metrics_collector_manager')->willReturn($mcm);
    $prometheusBridge->setContainer($container->reveal());
    $this->prometheusBridge = $prometheusBridge;
  }

}
