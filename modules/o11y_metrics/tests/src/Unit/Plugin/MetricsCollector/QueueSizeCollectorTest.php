<?php

namespace Drupal\Tests\o11y_metrics\Unit\Plugin\MetricsCollector;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\o11y_metrics\Plugin\MetricsCollector\QueueSizeCollector;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\o11y_metrics\Plugin\MetricsCollector\QueueSizeCollector
 * @group o11y_metrics
 */
class QueueSizeCollectorTest extends AbstractTestBaseMetrics {

  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics() {

    $queue1 = $this->prophesize(QueueInterface::class);
    $queue1->numberOfItems()->willReturn(12);

    $queue2 = $this->prophesize(QueueInterface::class);
    $queue2->numberOfItems()->willReturn(42);

    $queueFactory = $this->prophesize(QueueFactory::class);
    $queueFactory->get(Argument::any())->willReturn($queue1, $queue2);

    $config = [
      'settings' => [
        'queues' => [
          'queue1' => 'queue1',
          'queue2' => 'queue2',
        ],
      ],
    ];
    $definition = [
      'provider' => 'test',
      'description' => 'Dummy description',
    ];
    $queuePluginManager = $this->prophesize(QueueWorkerManagerInterface::class);
    $queuePluginManager->getDefinitions()->willReturn([
      'queue1' => 'queue1',
      'queue2' => 'queue2',
    ]);
    $collector = new QueueSizeCollector($config, 'queue_size_test', $definition, $this->prometheusBridge, $queueFactory->reveal(), $queuePluginManager->reveal());
    $collector->executeMetrics();

    $this->assertEquals(<<<EOD
# HELP drupal_queue_size_test_total Dummy description
# TYPE drupal_queue_size_test_total gauge
drupal_queue_size_test_total{queue="queue1"} 12
drupal_queue_size_test_total{queue="queue2"} 42
EOD, $this->prometheusBridge->render());

  }

}
