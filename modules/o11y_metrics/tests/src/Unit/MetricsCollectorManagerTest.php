<?php

namespace Drupal\Tests\o11y_metrics\Unit;

use Drupal\o11y_metrics\MetricsCollectorManager;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\o11y_metrics\MetricsCollectorManager
 * @group o11y_metrics
 */
class MetricsCollectorManagerTest extends UnitTestCase {

  /**
   * @covers ::syncPluginConfig
   */
  public function testSync() {

    $config = [
      'node_count' => [
        'id' => 'node_count',
        'provider' => 'o11y_metrics',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [
          'bundles' => [
            'article' => 'article',
          ],
        ],
      ],
      'mock_removed' => [
        'id' => 'mock_removed',
        'provider' => 'o11y_metrics',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ],
    ];

    $plugin_ids = ['mock_added', 'node_count'];

    $actual_config = MetricsCollectorManager::calculateSyncCollectorConfig($config, $plugin_ids);

    $expected_config = [
      'mock_added' => [
        'id' => 'mock_added',
        'provider' => 'o11y_metrics',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ],
      'node_count' => [
        'id' => 'node_count',
        'provider' => 'o11y_metrics',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [
          'bundles' => [
            'article' => 'article',
          ],
        ],
      ],
    ];

    // Assert that removed plugin config is gone and new plugins are added.
    $this->assertEquals($expected_config, $actual_config);

  }

}
