<?php

namespace Drupal\Tests\o11y_metrics\Kernel;

use Symfony\Component\HttpFoundation\Request;

/**
 * Tests for metrics.
 *
 * @group o11y_metrics
 */
class MetricsEndpointTest extends PrometheusExporterKernelTestBase {

  /**
   * The test user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->createUser(['access prometheus metrics']);
    $this->setCurrentUser($this->user);
  }

  /**
   * Tests the metrics endpoint.
   */
  public function testMetrics() {
    $request = Request::create('/metrics');
    $response = $this->httpKernel->handle($request)->getContent();

    $this->assertStringContainsString('# HELP drupal_test_total A test collector.', $response);
    $this->assertStringContainsString('# TYPE drupal_test_total gauge', $response);
    $this->assertStringContainsString('drupal_test_total{foo="bar"} 1234', $response);
  }

  /**
   * Tests the metrics endpoint with disabled test-collector.
   */
  public function testDisabledMetrics() {
    $config = $this->config('o11y_metrics.settings');
    $config->set('collectors.test.enabled', FALSE);
    $config->save();

    $request = Request::create('/metrics');
    $response = $this->httpKernel->handle($request)->getContent();

    $this->assertStringNotContainsString('# HELP drupal_test_total A test collector.', $response);
    $this->assertStringNotContainsString('# TYPE drupal_test_total gauge', $response);
  }

}
