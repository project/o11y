<?php

namespace Drupal\Tests\o11y_metrics\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Abstract base class for Prometheus Exporter kernel tests.
 */
abstract class PrometheusExporterKernelTestBase extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'o11y_metrics',
    'o11y_metrics_update',
    'o11y_metrics_test',
    'user',
    'node',
    'update',
    'system',
    'pcb',
  ];

  /**
   * Kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);
    $def = $container->getDefinition('cache.prometheusio_bin');
    $factory = $def->getFactory();
    $factory[0] = new Reference('cache.backend.permanent_database');
    $def->setFactory($factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setInstallProfile('minimal');
    $this->installConfig(['user']);
    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');

    $this->httpKernel = $this->container->get('http_kernel');

    $config = $this->config('o11y_metrics.settings');
    $settings = [];
    foreach ([
      'phpinfo',
      'node_count',
      'user_count',
      'revision_count',
      'queue_size',
      'update_status',
      'extensions',
      'test',
    ] as $item) {
      $settings[$item] = ['enabled' => TRUE];
    }
    $config->set('collectors', $settings);
    $config->save();
  }

}
