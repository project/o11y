<?php

namespace Drupal\Tests\o11y_metrics\Kernel\Form;

use Drupal\Tests\o11y_metrics\Kernel\PrometheusExporterKernelTestBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\o11y_metrics\Form\PrometheusExporterPluginSettings
 * @group o11y_metrics
 */
class PrometheusExporterSettingsTest extends PrometheusExporterKernelTestBase {

  /**
   * The user for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->createUser(['administer prometheus exporter settings']);
    $this->setCurrentUser($this->user);
  }

  /**
   * Tests the metrics endpoint.
   */
  public function testSettingsForm() {
    $request = Request::create('/admin/config/system/o11y_metrics/plugins-settings');
    $response = $this->httpKernel->handle($request)->getContent();

    $this->assertStringContainsString("Plugins settings", $response);

    // @todo add form assertions.
  }

}
