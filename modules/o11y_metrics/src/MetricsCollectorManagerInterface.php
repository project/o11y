<?php

namespace Drupal\o11y_metrics;

/**
 * Collects metrics for export to prometheus.
 */
interface MetricsCollectorManagerInterface {

  /**
   * Gather metrics for each collector of type plugin.
   */
  public function executeMetricsPlugins();

  /**
   * Syncs the plugin config removing any obsolete config.
   *
   * @see o11y_metrics_modules_uninstalled()
   */
  public function syncPluginConfig();

  /**
   * Calculates what the syncd config should be.
   *
   * @param array $config
   *   An associative array of current collector config.
   * @param string[] $plugin_ids
   *   The plugin IDs of newly installed or uninstalled collector plugins.
   *
   * @return array
   *   The updated config.
   */
  public static function calculateSyncCollectorConfig(array $config, array $plugin_ids);

  /**
   * Gets the metrics collector plugins.
   *
   * @return \Drupal\o11y_metrics\MetricsCollectorPluginCollection
   *   The plugins.
   */
  public function getPlugins();

}
