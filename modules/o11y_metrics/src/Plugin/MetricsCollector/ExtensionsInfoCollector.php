<?php

namespace Drupal\o11y_metrics\Plugin\MetricsCollector;

use Drupal\o11y_metrics\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for the drupal extensions.
 *
 * @MetricsCollector(
 *   id = "extensions",
 *   title = @Translation("Extensions"),
 *   description = @Translation("Info about modules, themes, profiles")
 * )
 */
class ExtensionsInfoCollector extends BasePluginMetricsCollector {

  /**
   * The drupal extension.list.module service.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $modules;

  /**
   * The drupal extension.list.theme service.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $themes;

  /**
   * The drupal extension.list.profiles service.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $profiles;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\Core\Extension\ExtensionList $modules
   *   The drupal extension.list.module service.
   * @param \Drupal\Core\Extension\ExtensionList $themes
   *   The drupal extension.list.theme service.
   * @param \Drupal\Core\Extension\ExtensionList $profiles
   *   The drupal extension.list.profile service.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    ExtensionList $modules,
    ExtensionList $themes,
    ExtensionList $profiles
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->modules = $modules;
    $this->themes = $themes;
    $this->profiles = $profiles;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o11y_metrics.prometheus_bridge'),
      $container->get('extension.list.module'),
      $container->get('extension.list.theme'),
      $container->get('extension.list.profile')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $this->moduleMetrics('modules');
    $this->moduleMetrics('themes');
    $this->moduleMetrics('profiles');
  }

  /**
   * Common metrics function.
   */
  protected function moduleMetrics($type) {
    $namespace_name_help = [
      $this->getNamespace(),
      $type,
      "Info about $type.",
    ];
    $gauge = $this->promBridge->getGauge(
      ...$namespace_name_help,
      ...[['name', 'version']]
    );
    $info = $this->{$type}->getAllInstalledInfo();
    foreach ($info as $machine_name => $struct) {
      $gauge->set(1, [$machine_name, $struct['version']]);
    }
    $namespace_name_help = [
      $this->getNamespace(),
      $type . '_count',
      "Count of $type.",
    ];
    $this->promBridge->getGauge(...$namespace_name_help)->set(count($info));
  }

}
