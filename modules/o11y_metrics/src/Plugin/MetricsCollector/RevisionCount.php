<?php

namespace Drupal\o11y_metrics\Plugin\MetricsCollector;

/**
 * Collects metrics for total revision count.
 *
 * @MetricsCollector(
 *   id = "revision_count",
 *   title = @Translation("Revision count"),
 *   description = @Translation("Total revision count.")
 * )
 */
class RevisionCount extends NodeCount {

  /**
   * {@inheritdoc}
   */
  protected function getCountQuery($bundle = NULL) {
    return parent::getCountQuery($bundle)->allRevisions();
  }

}
