<?php

namespace Drupal\o11y_metrics\Plugin\MetricsCollector;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\o11y_metrics\Plugin\BasePluginMetricsCollector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface;

/**
 * Collects metrics for the total node count.
 *
 * @MetricsCollector(
 *   id = "node_count",
 *   title = @Translation("Node count"),
 *   description = @Translation("Total node count.")
 * )
 */
class NodeCount extends BasePluginMetricsCollector {

  /**
   * The node type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface $promBridge
   *   The promphp bridge.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node type storage.
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrometheusBridgeInterface $promBridge,
    EntityStorageInterface $node_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $promBridge);
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o11y_metrics.prometheus_bridge'),
      $container->get('entity_type.manager')->getStorage('node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = node_type_get_names();
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node Types'),
      '#decription' => $this->t('Choose which node types you want to expose metrics for.'),
      '#default_value' => $this->getConfiguration()['settings']['bundles'] ?? [],
      '#options' => $options,
    ];
    if (empty($options)) {
      $form['bundles']['#markup'] = '<em>' . $this->t('No node types found') . '</em>';
    }
    return $form;
  }

  /**
   * Gets a count query for this metric.
   *
   * @param string $bundle
   *   (optional) The bundle name to filter by.
   */
  protected function getCountQuery($bundle = NULL) {
    $query = $this->nodeStorage->getQuery();
    $query->accessCheck(FALSE);
    if ($bundle) {
      $query->condition('type', $bundle);
    }
    return $query->count();
  }

  /**
   * Gets a count for this metric.
   *
   * @param string $bundle
   *   (optional) The bundle name to filter by.
   *
   * @return int
   *   The node count.
   */
  protected function getCount($bundle = NULL) {
    return $this->getCountQuery($bundle)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetrics() {
    $namespace_name_help = [
      $this->getNamespace(),
      'total',
      $this->getDescription(),
    ];
    $this->promBridge->getGauge(...$namespace_name_help)->set($this->getCount());
    $bundles = $this->getConfiguration()['settings']['bundles'] ?? [];
    array_map(function ($bundle) use ($namespace_name_help) {
      $namespace_name_help[1] = 'total_per_bundle';
      $this->promBridge->getGauge(...$namespace_name_help, ...[['bundle']])->set($this->getCount($bundle), [$bundle]);
    }, array_filter($bundles, function ($bundle) {
      // Disabled bundles have a value of 0.
      return $bundle !== 0;
    }));
  }

}
