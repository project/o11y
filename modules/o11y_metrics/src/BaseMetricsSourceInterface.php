<?php

namespace Drupal\o11y_metrics;

/**
 * Interface for non-plugin collectors.
 *
 * I.e. collectors whose metrics need to be linked to a source id.
 */
interface BaseMetricsSourceInterface {

  /**
   * Returns a string identifying the metrics source.
   */
  public static function getMetricsSourceId(): string;

}
