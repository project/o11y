<?php

namespace Drupal\o11y_metrics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface;

/**
 * A controller for exporting prometheus metrics.
 */
class MetricsController extends ControllerBase {

  /**
   * The promPHP bridge.
   *
   * @var \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface
   */
  protected $promBridge;

  /**
   * MetricsController constructor.
   *
   * @param \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface $promBridge
   *   The promPHP bridge.
   */
  final public function __construct(PrometheusBridgeInterface $promBridge) {
    $this->promBridge = $promBridge;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('o11y_metrics.prometheus_bridge')
    );
  }

  /**
   * Handles metrics requests.
   */
  public function metrics() {
    $response = new Response();
    $response->setMaxAge(0);
    $response->headers->set('Content-Type', 'text/plain; version=0.0.4');
    $response->setContent($this->promBridge->render());
    return $response;
  }

}
