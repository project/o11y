<?php

namespace Drupal\o11y_metrics\Prometheus;

use Drupal\o11y_metrics\BaseMetricsSourceInterface;
use Prometheus\Collector;
use Prometheus\Storage\Adapter;
use Prometheus\CollectorRegistry;

/**
 * CollectorRegistry for non-plugins.
 *
 * Same as collector registry for plugins but has also the capability to relate
 * a source to a set of metrics.
 */
class CollectorRegistryForNonPlugins extends CollectorRegistry implements RelatesSourceToMetricsInterface {

  /**
   * The storage adapter for prometheus metrics.
   *
   * @var \Drupal\o11y_metrics\Prometheus\RelatesSourceToMetricsInterface
   */
  protected $storageAdapter;

  /**
   * {@inheritdoc}
   */
  public function __construct(Adapter $storageAdapter, bool $registerDefaultMetrics = TRUE) {
    if (!($storageAdapter instanceof RelatesSourceToMetricsInterface)) {
      throw new \InvalidArgumentException('Parameter $storageAdapter of ' . __METHOD__ . ' should implement interface ' . RelatesSourceToMetricsInterface::class);
    }

    $this->storageAdapter = $storageAdapter;
    parent::__construct($storageAdapter, $registerDefaultMetrics);
  }

  /**
   * {@inheritdoc}
   */
  public function associateSourceToMetric(
    BaseMetricsSourceInterface $metricsSource,
    Collector $metric
  ) {
    $this->storageAdapter->associateSourceToMetric($metricsSource, $metric);
  }

  /**
   * {@inheritdoc}
   */
  public function removeMetricsOfSource(string $metricsSourceId) {
    $this->storageAdapter->removeMetricsOfSource($metricsSourceId);
  }

  /**
   * {@inheritdoc}
   */
  public function hasMetricsOfSource(string $metricsSourceId) {
    return $this->storageAdapter->hasMetricsOfSource($metricsSourceId);
  }

}
