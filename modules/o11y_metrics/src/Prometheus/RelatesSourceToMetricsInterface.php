<?php

namespace Drupal\o11y_metrics\Prometheus;

use Drupal\o11y_metrics\BaseMetricsSourceInterface;
use Prometheus\Collector;

/**
 * Defines something that can relate a source to a metric.
 */
interface RelatesSourceToMetricsInterface {

  /**
   * Associate a source id to a metric name.
   */
  public function associateSourceToMetric(
    BaseMetricsSourceInterface $metricsSource,
    Collector $metric
  );

  /**
   * Removes metrics belonging to source.
   */
  public function removeMetricsOfSource(string $metricsSourceId);

  /**
   * Gets the metrics of source.
   */
  public function hasMetricsOfSource(string $metricsSourceId);

}
