<?php

namespace Drupal\o11y_metrics;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * {@inheritdoc}
 */
class MetricsCollectorManager implements MetricsCollectorManagerInterface {

  const CONFIG_NAME = 'o11y_metrics.settings';

  /**
   * The metrics collector plugin collection.
   *
   * @var \Drupal\o11y_metrics\MetricsCollectorPluginCollection
   */
  protected $pluginCollection;

  /**
   * The settings, as defined in backend.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * MetricsCollectorCollector constructor.
   *
   * @param \Drupal\o11y_metrics\MetricsCollectorPluginManager $pluginManager
   *   The plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configuration
   *   The configurationFactory.
   */
  public function __construct(MetricsCollectorPluginManager $pluginManager, ConfigFactoryInterface $configuration) {
    $this->configFactory = $configuration;
    $this->settings = $configuration->get(static::CONFIG_NAME);
    $collectorsSettings = $this->settings->get('collectors');
    $definitions = [];
    foreach ($pluginManager->getDefinitions() as $id => $definition) {
      $settings = $collectorsSettings[$id] ?? [];
      $definitions[$id] = array_merge($definition, $settings);
    }
    $this->pluginCollection = new MetricsCollectorPluginCollection($pluginManager, $definitions);
    $this->pluginCollection->sort();
  }

  /**
   * {@inheritdoc}
   */
  public function executeMetricsPlugins() {
    /** @var \Drupal\o11y_metrics\Plugin\PluginMetricsCollectorInterface $collector */
    foreach ($this->pluginCollection->getIterator() as $collector) {
      if ($collector->isEnabled() && $collector->applies()) {
        $collector->executeMetrics();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncPluginConfig() {
    $plugin_ids = $this->pluginCollection->getInstanceIds();
    $settings = $this->configFactory->getEditable(static::CONFIG_NAME);
    $config = $settings->get('collectors');
    // Check if any plugins have been installed or uninstalled.
    $updated_config = self::calculateSyncCollectorConfig($config, $plugin_ids);
    $settings->set('collectors', $updated_config);
    $settings->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateSyncCollectorConfig(array $config, array $plugin_ids) {
    // Remove any config for removed plugins.
    $updated_config = array_intersect_key($config, array_flip($plugin_ids));
    $new_config_keys = array_diff_key(array_flip($plugin_ids), $config);
    foreach (array_keys($new_config_keys) as $key) {
      $updated_config[$key] = [
        'id' => $key,
        'provider' => 'o11y_metrics',
        'enabled' => FALSE,
        'weight' => 0,
        'settings' => [],
      ];
    }
    return $updated_config;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugins() {
    return $this->pluginCollection;
  }

}
