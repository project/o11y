<?php

namespace Drupal\o11y_metrics\Bridge;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Drupal\o11y_metrics\BaseMetricsSourceInterface;

/**
 * Bridge between this drupal module and prometheus lib.
 */
interface PrometheusBridgeInterface extends ContainerAwareInterface {

  /**
   * Render all the metrics.
   */
  public function render(): string;

  /**
   * Convenient function to get a counter.
   */
  public function getCounter(
    string $namespace,
    string $name,
    string $help,
    array $labels = [],
    BaseMetricsSourceInterface $metricsSource = NULL
  );

  /**
   * Convenient function to get a histogram.
   */
  public function getHistogram(
    string $namespace,
    string $name,
    string $help,
    array $labels = [],
    array $buckets = NULL,
    BaseMetricsSourceInterface $metricsSource = NULL
  );

  /**
   * Convenient function to get a gauge.
   */
  public function getGauge(
    string $namespace,
    string $name,
    string $help,
    array $labels = [],
    BaseMetricsSourceInterface $metricsSource = NULL
  );

  /**
   * Convenient function to get a summar.
   */
  public function getSummary(
    string $namespace,
    string $name,
    string $help,
    array $labels = [],
    int $maxAgeSeconds = 600,
    array $quantiles = NULL,
    BaseMetricsSourceInterface $metricsSource = NULL
  );

  /**
   * Removes prometheus metrics belonging to source.
   *
   * Passing the class name to avoid instanciation.
   *
   * @param string $metricsSourceClassName
   *   The class name of the metrics source, must implement
   *   \Drupal\o11y_metrics\BaseMetricsSourceInterface.
   */
  public function removeMetricsOfSource(string $metricsSourceClassName);

  /**
   * Gets the metrics of source.
   */
  public function hasMetricsOfSource(string $metricsSourceClassName);

  /**
   * Wipes metrics.
   */
  public function wipeMetrics();

}
