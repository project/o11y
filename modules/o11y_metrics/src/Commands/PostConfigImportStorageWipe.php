<?php

namespace Drupal\o11y_metrics\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;
use Consolidation\AnnotatedCommand\CommandData;
use Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface;
use Drupal\o11y_metrics\MetricsCollectorManager;

/**
 * A Drush commandfile for Prometheus Exporter.
 */
class PostConfigImportStorageWipe extends DrushCommands {

  /**
   * The promphp bridge.
   *
   * @var \Drupal\o11y_metrics\Bridge\PrometheusBridgeInterface
   */
  protected $promBridge;

  /**
   * The cron configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $drupalConfig;

  /**
   * PostConfigImportStorageWipe constructor.
   */
  public function __construct(
    PrometheusBridgeInterface $promBridge,
    ConfigFactoryInterface $config_factory
  ) {
    $this->promBridge = $promBridge;
    $this->drupalConfig = $config_factory->get(MetricsCollectorManager::CONFIG_NAME);
  }

  /**
   * Hooks after the config:import command to wipe (client) prometheus storage.
   *
   * A config:import usually identifies a deploy, here we wipe the client
   * metrics storage to start fresh.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   * @hook post-command config:import
   */
  public function postConfigImport($result, CommandData $commandData) {
    if ($this->drupalConfig->get('wipe_client_storage_on_drush_config_import') ?? TRUE) {
      $this->promBridge->wipeMetrics();
      $commandData->output()->writeln('Wiped client prometheus storage');
    }
  }

}
