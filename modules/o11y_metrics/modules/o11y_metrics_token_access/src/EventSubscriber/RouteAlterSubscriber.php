<?php

namespace Drupal\o11y_metrics_token_access\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter the metrics route to add our own access control.
 */
class RouteAlterSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('o11y_metrics.metrics')) {
      $route->setRequirements([
        '_prometheus_token_access' => 'true',
      ]);
    }
  }

}
