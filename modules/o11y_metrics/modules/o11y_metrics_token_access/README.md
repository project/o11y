# Prometheus Exporter Token Access

This module allows access to the `/metrics` endpoint by providing a token as a query string parameter.

Example configuration in `settings.php`:

```php
$config['o11y_metrics_token_access.settings']['access_token'] = "abcd1234";
```

Example url:

https://example.com/metrics?token=abcd1234
