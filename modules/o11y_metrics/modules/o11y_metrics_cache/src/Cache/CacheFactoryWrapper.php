<?php

namespace Drupal\o11y_metrics_cache\Cache;

use Drupal\Core\Cache\CacheFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Decorates cache_factory.
 */
class CacheFactoryWrapper implements CacheFactoryInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * The decorated cache factory.
   *
   * @var \Drupal\Core\Cache\CacheFactoryInterface
   */
  protected $cacheFactory;

  /**
   * The bins to exclude from our work.
   *
   * @var array
   */
  protected $binsBlacklist;

  /**
   * All wrapped cache backends.
   *
   * @var \Drupal\o11y_metrics_cache\Cache\CacheBackendWrapper[]
   */
  protected $cacheBackends = [];

  /**
   * Creates a new CacheFactoryWrapper instance.
   *
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   * @param string[] $binsBlacklist
   *   A list of cache bins to exclude.
   */
  public function __construct(
    CacheFactoryInterface $cache_factory,
    array $binsBlacklist
  ) {
    $this->cacheFactory = $cache_factory;
    $this->binsBlacklist = $binsBlacklist;
    $this->cacheBackends = [];
  }

  /**
   * {@inheritdoc}
   */
  public function get($bin) {
    if (!$this->isEnabled($bin)) {
      // If disabled, return an unwrapped backend.
      return $this->cacheFactory->get($bin);
    }

    if (!isset($this->cacheBackends[$bin])) {
      $cache_backend = $this->cacheFactory->get($bin);
      $this->cacheBackends[$bin] = new CacheBackendWrapper($cache_backend, $bin, $this->container->get('o11y_metrics.prometheus_bridge'));
    }
    return $this->cacheBackends[$bin];
  }

  /**
   * Whether the factory in enable for the bin.
   *
   * Use services.yml parameter to disable logging for certain bins, or '*' for
   * all bins.
   *
   * @param string $bin
   *   The bin name to check.
   *
   * @return bool
   *   Whether the bin is enabled or not.
   */
  public function isEnabled($bin) {
    return !in_array('*', $this->binsBlacklist) && !in_array($bin, $this->binsBlacklist);
  }

}
