<?php

namespace Drupal\o11y_metrics_requests\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\o11y_metrics\Traits\FormWithBuckets;
use Drupal\o11y_metrics_requests\EventSubscriber\RequestCollector;
use Prometheus\Histogram;

/**
 * Form to configure settings of o11y_metrics_requests.
 */
class RequestsSettingsForm extends ConfigFormBase {

  use FormWithBuckets;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [RequestCollector::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'o11y_metrics_requests_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $form['exclude_admin_paths'] = $this->getExcludeAdminPathsFormElement($config->get('exclude_admin_paths') ?? TRUE);
    $form['buckets'] = $this->getBucketsFormElement(
      'requests',
      'seconds',
      RequestCollector::class,
      Histogram::getDefaultBuckets(),
      $config->get('buckets') ?? []
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validates that the buckets are float and in increasing order.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->validateBucketsValue($form_state, 'buckets');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config
      ->set('buckets', $this->parseEolSeparatedValue($form_state->getValue('buckets') ?? ''))
      ->set('exclude_admin_paths', $form_state->getValue('exclude_admin_paths'))
      ->save();
    // We need to clean data in storage to be able to change the buckets.
    $this->promBridge->removeMetricsOfSource(RequestCollector::class);
    parent::submitForm($form, $form_state);
  }

}
