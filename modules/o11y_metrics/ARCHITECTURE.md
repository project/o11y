# Architecture

## The PromPHP library and the bridge

In order to store, compute and serialize the prometheus entities
(gauges, counters, histograms and summaries) we use the
[PromPHP/prometheus_client_php](https://github.com/PromPHP/prometheus_client_php) library.

The `Drupal\o11y_metrics\Bridge\PrometheusBridge` class is a bridge
between Drupal and the library.

## Collectors

Metrics collectors are classes that call `getGauge()`/`getCounter()`/`getHistogram()`/`getSummary()`
on a `Drupal\o11y_metrics\Bridge\PrometheusBridge` object in order to register prometheus metrics.

Metrics collectors are grouped into two categories, plugin collectors and non-plugin collectors.

## Pluging collectors

These are classes in the `Drupal\...\Plugin\MetricsCollector` namespace.

These classes have their `executeMetrics` method called during a request on the
prometheus main scraping route (`/metrics`).

So it is usefult to implement a collector as a plugin when:

  - the metrics they want to collect can be extracted during a single moment in
    time (the scraping event).
  - it is computationally cheap to call `executeMetrics` during scraping.

## Non-plugin collectors

By exclusion, these are all the other metrics collectors.
Currently we have 3 non-plugin collectors:

- database: `Drupal\o11y_metrics_database\EventSubscriber\DatabaseCollector`
- requests: `Drupal\o11y_metrics_requests\EventSubscriber\RequestCollector`
- cache: `Drupal\o11y_metrics_cache\Cache\CacheMetricsCacheTagsInvalidator`
     and `Drupal\o11y_metrics_cache\Cache\CacheBackendWrapper`

These collectors need to gather data constantly, not only during the scraping
event so they are integrated into the Drupal system via specific means (an
EventSubscriber, a decoration of a core service, an http middleware...).

They store their metrics in a persistent storage system (by default we use the
drupal cache storage implementing the `Prometheus\Storage\Adapter` of
[https://github.com/PromPHP/prometheus_client_php](PromPHP/prometheus_client_php))
