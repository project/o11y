<?php

declare(strict_types=1);

namespace Drupal\o11y_traces\Http;

use Drupal\Core\Http\ClientFactory as CoreClientFactory;
use OpenTelemetry\API\Trace\Propagation\TraceContextPropagator;

/**
 * Extends the default HTTP client factory to inject the trace context.
 */
class ClientFactory extends CoreClientFactory {

  /**
   * {@inheritdoc}
   */
  public function fromOptions(array $config = []) {
    $headers = [];

    $propagator = new TraceContextPropagator();
    $propagator->inject($headers);

    return parent::fromOptions(['headers' => $headers]);
  }

}
