<?php

declare(strict_types=1);

namespace Drupal\o11y_traces\Plugin\Tracer;

use Drupal\tracer\TracerInterface;
use OpenTelemetry\API\Trace\SpanInterface;
use OpenTelemetry\API\Trace\TracerInterface as OTELTracerInterface;
use OpenTelemetry\Contrib\OtlpHttp\Exporter;
use OpenTelemetry\SDK\Common\Time\SystemClock;
use OpenTelemetry\SDK\Trace\SpanProcessor\BatchSpanProcessor;
use OpenTelemetry\SDK\Trace\SpanProcessor\SimpleSpanProcessor;
use OpenTelemetry\SDK\Trace\TracerProvider;

/**
 * Tracer that uses OpenTelemetry as a backend.
 *
 * @Tracer(
 *   id = "o11y_tracer",
 *   label = @Translation("OpenTelemetry Tracer"),
 *   description = @Translation("OpenTelemetry Tracer"),
 *   )
 */
class OpenTelemetryTracer implements TracerInterface {

  /**
   * The OpenTelemetry Tracer.
   *
   * @var \OpenTelemetry\API\Trace\TracerInterface
   */
  private OTELTracerInterface $tracer;

  /**
   * OpenTelemetryTracer constructor.
   */
  public function __construct() {
    $tracerProvider = new TracerProvider(
      new BatchSpanProcessor(
        Exporter::fromConnectionString(),
        new SystemClock(),
      )
    );

    $this->tracer = $tracerProvider->getTracer('drupal.o11y');
  }

  /**
   * {@inheritdoc}
   */
  public function start(string $category, string $name, array $attributes = []): object {
    $attributes['name'] = $name;

    return $this->tracer->spanBuilder($category)
      ->setAttributes($attributes)
      ->startSpan();
  }

  /**
   * {@inheritdoc}
   */
  public function openSection(object $span): object {
    $span->activate();

    return $span;
  }

  /**
   * {@inheritdoc}
   */
  public function closeSection(object $span): object {
    return $span;
  }

  /**
   * {@inheritdoc}
   */
  public function stop(object $span): void {
    assert($span instanceof SpanInterface);
    $span->end();
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents(): array {
    return [];
  }

}
