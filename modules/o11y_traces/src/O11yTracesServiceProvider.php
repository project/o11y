<?php

declare(strict_types=1);

namespace Drupal\o11y_traces;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Defines a service profiler for the o11y_traces module.
 */
class O11yTracesServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('http_client_factory')
      ->setClass('Drupal\o11y_traces\Http\ClientFactory');
  }

}
