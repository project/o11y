<?php

namespace Drupal\o11y_traces\Logger\Processor;

use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;
use OpenTelemetry\API\Trace\AbstractSpan;

/**
 * Monolog processor to add OpenTelemetry traceID to the log record.
 */
class TracerProcessor implements ProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function __invoke(LogRecord $record): LogRecord {
    $spanContext = AbstractSpan::getCurrent()->getContext();

    $record->extra = array_merge(
      $record->extra,
      [
        'traceID' => $spanContext->getTraceId(),
      ]
    );

    return $record;
  }

}
