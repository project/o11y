<?php

namespace Drupal\o11y_traces\EventSubscriber;

use OpenTelemetry\API\Trace\AbstractSpan;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Add TraceId to response headers.
 */
class HeaderSubscriber implements EventSubscriberInterface {

  /**
   * Name of TraceId response header.
   */
  const HEADER = 'X-Drupal-Trace-Id';

  /**
   * Add TraceId to response headers.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    $spanContext = AbstractSpan::getCurrent()->getContext();

    $response->headers->set(self::HEADER, $spanContext->getTraceId());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onResponse', 100];

    return $events;
  }

}
